#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# enable debugging
import cgitb
import os, sys
cgitb.enable()
sys.path.append(os.path.join(os.path.dirname(__file__), 'lib'))
import functools
import json
import logging
import traceback
from lib.flask import Flask, jsonify, request, Response, redirect
import lib.youtube
from lib.youtube.utils import compat_urllib_parse
if not hasattr(sys.stderr, 'isatty'):
    # In GAE it's not defined and we must monkeypatch
    sys.stderr.isatty = lambda: False

class SimpleYDL(lib.youtube.YoutubeDL):
    def __init__(self, *args, **kargs):
        super(SimpleYDL, self).__init__(*args, **kargs)
        self.add_default_info_extractors()

def get_videos(url):
    '''
    Get a list with a dict for every video founded
    '''
    ydl_params = {
        'cachedir': None,
        'logger': app.logger.getChild('youtube-dl'),
    }
    ydl = SimpleYDL(ydl_params)
    res = ydl.extract_info(url, download=False)
    return res

def flatten_result(result):
    r_type = result.get('_type', 'video')
    if r_type == 'video':
        videos = [result]
    elif r_type == 'playlist':
        videos = []
        for entry in result['entries']:
            videos.extend(flatten_result(entry))
    elif r_type == 'compat_list':
        videos = []
        for r in result['entries']:
            videos.extend(flatten_result(r))
    return videos

app = Flask(__name__)
@app.route('/')
def index():
    url = request.args['url']
    errors = (lib.youtube.utils.DownloadError, lib.youtube.utils.ExtractorError)
    try:
        result = get_videos(url)
        key = 'info'
        # Turn it on by default to keep backwards compatibility.
        if request.args.get('flatten', 'True').lower() == 'true':
            result = flatten_result(result)
            key = 'videos'
        result = {
            'ytbdl.version': lib.youtube.__version__,
            'url': url,
            key: result,
        }
        return jsonify(result)
    except errors as err:
        logging.error(traceback.format_exc())
        result = jsonify({'error': str(err)})
        result.status_code = 500
        return result

if __name__ == '__main__':
     app.run(debug=True)
